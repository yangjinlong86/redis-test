package org.nocoder.redis.test;

import java.util.ArrayList;
import java.util.List;

import org.nocoder.redis.entity.User;
import org.nocoder.redis.util.RedisUtils;

public class TestRedis {
	public static void main(String[] args) {
		RedisUtils.setList("userList", getUserList());
		@SuppressWarnings("unchecked")
		List<User> userList = (List<User>) RedisUtils.getList("userList");
		System.out.println(userList.get(0).getName());
	}

	public static List<User> getUserList() {
		List<User> userList = new ArrayList<User>();
		for (int i = 0; i < 100; i++) {
			User user = new User();
			user.setName("name_" + i);
			user.setPassword("passwd_" + i);
			userList.add(user);
		}
		return userList;
	}
}
