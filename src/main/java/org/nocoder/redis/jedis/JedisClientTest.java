package org.nocoder.redis.jedis;

import org.junit.Test;

import redis.clients.jedis.Jedis;
import redis.clients.jedis.JedisPool;
import redis.clients.jedis.JedisPoolConfig;

public class JedisTest {
	/**
	 * Jedis 客户端测试
	 */
	@Test
	public void testJedis() {
		// ip 端口
		Jedis jedis = new Jedis("127.0.0.1", 6379);
		// 密码
		jedis.auth("fredaredis");
		// 保存数据
		jedis.set("name", "jason");
		// 获取数据
		String value = jedis.get("name");
		System.out.println(value);
		// 释放资源
		jedis.close();
	
	}
	
	/**
	 * JedisPool 连接池测试 
	 */
	@Test
	public void testJedisPool() {
		JedisPoolConfig config = new JedisPoolConfig();
		// 最大连接数
		config.setMaxTotal(30);
		// 最大空闲连接数
		config.setMaxIdle(10);
		
		JedisPool jedisPool = new JedisPool(config, "127.0.0.1", 6379);
		
		Jedis jedis = null;
		
		// 通过连接池获得链接
		jedis = jedisPool.getResource();
		jedis.auth("fredaredis");
		jedis.set("name", "freda");
		// 获取数据
		System.out.println(jedis.get("name"));
		// 释放资源
		jedis.close();
		// 释放连接池资源
		jedisPool.close();
	}
	
}
