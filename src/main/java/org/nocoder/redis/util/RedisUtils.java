package org.nocoder.redis.util;

import java.util.List;

import redis.clients.jedis.Jedis;

/**
 * Redis工具类
 * 
 * @author YANGJINLONG
 *
 */
public class RedisUtils {

	public static void setList(String key, List<?> list) {
		if (list == null || list.size() == 0) {
			getJedis().set(key.getBytes(), "".getBytes());
		} else {
			getJedis().set(key.getBytes(), SerializeUtils.serializeList(list));
		}
	}

	public static List<?> getList(String key) {
		if (getJedis() == null || !getJedis().exists(key)) {
			return null;
		}

		byte[] bytes = getJedis().get(key.getBytes());

		return SerializeUtils.unserializeList(bytes);
	}

	public static Jedis getJedis() {
		Jedis jedis = new Jedis("45.58.52.59");
		jedis.auth("jasonredis");
		return jedis;
	}
}
