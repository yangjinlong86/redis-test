package org.nocoder.redis.util;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.util.ArrayList;
import java.util.List;

public class SerializeUtils {

	/**
	 * 对象序列化
	 *
	 * @param obj
	 * @return
	 */
	public static byte[] serialize(Object obj) {

		ObjectOutputStream oos = null;

		ByteArrayOutputStream baos = new ByteArrayOutputStream();

		try {
			oos = new ObjectOutputStream(baos);

			oos.writeObject(obj);
		} catch (IOException e) {
			e.printStackTrace();
		}

		return baos.toByteArray();
	}

	/**
	 * 反序列化
	 *
	 * @param bytes
	 * @return
	 */
	public static Object unserialize(byte[] bytes) {
		ObjectInputStream ois = null;
		ByteArrayInputStream bais = new ByteArrayInputStream(bytes);
		Object obj = null;
		try {
			ois = new ObjectInputStream(bais);
			obj = ois.readObject();
		} catch (IOException e) {
			e.printStackTrace();
		} catch (ClassNotFoundException e) {
			e.printStackTrace();
		}

		return obj;
	}

	/**
	 * List对象序列化
	 *
	 * @param list
	 * @return bytes
	 */
	public static byte[] serializeList(List<?> list) {
		if (list == null || list.size() == 0) {
			return null;
		}

		ByteArrayOutputStream baos = new ByteArrayOutputStream();
		try {
			ObjectOutputStream oos = new ObjectOutputStream(baos);
			for (Object obj : list) {
				oos.writeObject(obj);
			}
		} catch (IOException e) {
			e.printStackTrace();
		}

		return baos.toByteArray();
	}

	/**
	 * List对象反序列化
	 *
	 * @param bytes
	 * @return list
	 */
	public static List<?> unserializeList(byte[] bytes) {
		List<Object> list = new ArrayList<Object>();
		ByteArrayInputStream bais = new ByteArrayInputStream(bytes);
		try {
			ObjectInputStream ois = new ObjectInputStream(bais);
			while (bais.available() > 0) {
				Object obj = ois.readObject();
				list.add(obj);
			}
		} catch (IOException e) {
			e.printStackTrace();
		} catch (ClassNotFoundException e) {
			e.printStackTrace();
		}
		return list;
	}

	public static void main(String[] args) {
		/*
		 * User user = new User(); user.setName("Jason Yang");
		 * user.setPassword("123456");
		 *
		 * byte[] bytes = SerializeUtils.serialize(user);
		 *
		 * System.out.println(bytes);
		 *
		 * User user1 = (User) SerializeUtils.unserialize(bytes);
		 * System.out.println(user1.getName());
		 * System.out.println(user1.getPassword());
		 */
	}
}
